'use client'
import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import { useCategories } from '@/queries/hooks/useCategories'

type Data = {
    types: {id:number,name:string,src:string}[]|undefined
    color: string|undefined,
    active: string[]|undefined,
    ingredients: {id:number,name:string,src:string}[]|[],
    setIngredients: React.Dispatch<React.SetStateAction<{id:number,name:string,src:string}[]|[]>>
}

const RightBoard = ({types,color,active,ingredients,setIngredients}:Data) => {

    const [token, setToken] = useState<any>()
    useEffect(() => {
        setToken(localStorage.getItem('token'))
    }, [])
    const condition = token === undefined ? false : true 
    const { data } = useCategories(token,condition)

    const categoryTypes = (category:any) => {
        return category.ingredients.map((item:any) => item.translation.name)
    } 

    const handleClickOnType = (typeId: number, typeName: string, typeSrc:string) => {  
        const one = data?.data.data.filter((item:any) => item.translation.name === color) 
        const flag = one[0].can_select_many
        if(flag === 1){
            const ingredientsNames = ingredients?.map((item) => item.name)
            if(ingredientsNames?.includes(typeName)){
                setIngredients(ingredients?.filter((value) => value.name !== typeName))
            }
            else{
                setIngredients([...ingredients,{id:typeId,name:typeName,src:typeSrc}])
            }
        }
        else {
            const ingredientsNames = ingredients?.map((item) => item.name)
            const contains = ingredientsNames.some(element => {
            return categoryTypes(one[0]).includes(element)
            })
            console.log(contains)
            if(contains){
                if(ingredientsNames.includes(typeName)){
                    ingredients.map((item) => item.name === typeName ? setIngredients(ingredients.filter((value) => value.name !== item.name)) : null)
                }
                else if (!ingredientsNames.includes(typeName)){
                    const replace = (index:number) => {
                        const newValue = Object.values({...ingredients,[index]:{id:typeId,name:typeName,src:typeSrc}})
                        // @ts-ignore comment above the line of code that is causing the error 
                        setIngredients(newValue)
                    }
                    categoryTypes(one[0]).map((item:any) => ingredientsNames.includes(item) ?
                                                            ingredients.map((value,index) => value.name === item ? 
                                                            replace(index)
                                                            : 
                                                            null
                                                            )  
                                                            : 
                                                            null 
                                                            )
                    // categoryTypes(one[0]).map((item:any) => ingredientsNames.includes(item) ? setIngredients(ingredients.filter((value) => value.name !== item)) : null)
                    // setIngredients(state => [...state,{id:typeId,name:typeName,src:typeSrc}])
                } 
            }
            else{
                setIngredients([...ingredients,{id:typeId,name:typeName,src:typeSrc}])
            }
        }
    }

    return(
        <> 
            <h1 className="flex justify-center mt-[5%] mb-[3%] text-gray-500 tracking-widest">CHOOSE YOUR {color?.toUpperCase()}</h1>
            <div className={`${types !== undefined ? types.length >= 16 ? "mb-[2rem]" : types.length >= 12 ? "mb-[6rem]" : types.length >= 8 ? "mb-[11.5rem]" : types.length >= 4 ? "mb-[17rem]" : "mb-[17rem]" : null} grid grid-cols-12 gap-y-3 gap-x-1 justify-items-center lg:px-4 xl:px-0`}>
                {
                    types?.map((item:any, index:any) => (
                        <div className="md:col-span-3 lg:col-span-4 xl:col-span-3 grid justify-items-center cursor-pointer" onClick={() => handleClickOnType(item.id,item.name,item.src)} key={index}>
                            <Image src={item.src} alt="ingredients_image" width={45} height={45} className={`w-14 h-14 mb-2 ${active?.includes(item.name) ? " p-2 border rounded-full shadow-md":null}`} />
                            <h1 className="text-[0.9rem] tracking-widest">{item.name}</h1>
                        </div>
                    ))  
                }
            </div>
        </>
    )
}

export default RightBoard