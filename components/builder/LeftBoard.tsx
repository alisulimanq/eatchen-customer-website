import React, { useState } from 'react'
import Image from 'next/image'
import { Tooltip, notification, Spin } from 'antd'
import { CgNotes } from 'react-icons/cg'
import { AiOutlineCloseCircle } from 'react-icons/ai'
import { baseUrl, custom_order } from '@/api/Api_Info'
import axios from 'axios'

type Data = {
  ingredients: {id:number,name:string,src:string}[]|[],
  noteFlag: boolean,
  nameMeal: string,
  setNameMeal: (nameMeal: string) => void,
  noteContent: string,
  setNoteContent: (noteContent: string) => void,
  setNoteFalg: (noteFlag: boolean) => void,
  setIngredients: React.Dispatch<React.SetStateAction<{id:number,name:string,src:string}[]|[]>>,
  setSuccessAddOrder: React.Dispatch<React.SetStateAction<boolean>>
}

type NotificationType = 'error'

const LeftBoard = ({ingredients,noteFlag,nameMeal,setNameMeal,noteContent,setNoteContent,setNoteFalg,setIngredients,setSuccessAddOrder}:Data) => {

  const [placeOrderLoad, setPlaceOrderLoad] = useState<boolean>(false)

  const handleNote = () => {
    setNoteFalg(!noteFlag)
    setNoteContent("")
  }

  const [api, contextHolder] = notification.useNotification()
  
  const openNotificationWithIcon = (type: NotificationType, message:string) => {
    api[type]({
      message,
    })
  }

  const handleClose = (item:any) => {
    setIngredients(ingredients?.filter((value) => value.name !== item.name))
  }

  const customOrderHandler = async () => {
    setPlaceOrderLoad(true)
    const token = localStorage.getItem('token')
    const config = {
      headers: { Authorization: `Bearer ${token}` }
    }
    const ingredients_ids = ingredients.map((ingredient) => ingredient.id)
    const data = {
      name: nameMeal,
      ingredients: ingredients_ids,
      notes: noteContent
    }
    await axios.post(`${baseUrl}${custom_order}`,data,config)
    .then(function(res) {
      setSuccessAddOrder(true)
      console.log(res)
      setPlaceOrderLoad(false)
    })
    .catch(function(err) {
      err.code === "ERR_NETWORK" ? openNotificationWithIcon("error","Connection Error") : null
      console.log(err.code)
      setPlaceOrderLoad(false)
    })
  }

  const clearHandler = () => {
    setIngredients([])
    setNoteContent("")
    setNameMeal("")
  }

  return (
    <>
      {contextHolder}
      <div className={`flex justify-between items-center w-[80%] ${ingredients?.length === 0 && noteFlag === false ? "mb-[16rem]" : null}`}>
        <input
          type="text" 
          value={nameMeal} 
          placeholder="Assign A Name For This Meal" 
          className="w-[90%] h-[4rem] border border-transparent shadow-md rounded-md pl-3"
          onChange={(e) => setNameMeal(e.target.value)}  
        />
        <Tooltip title="There is any note ?">
          <CgNotes className={`text-[2.4rem] ${noteFlag === true ? "text-C3":"text-gray-400"} cursor-pointer`} onClick={() => handleNote()}/>                   
        </Tooltip>
      </div>
      <input 
        type="text" 
        value={noteContent} 
        placeholder="Add A Note For This Order" 
        className={`${noteFlag === true ? "flex" : "hidden"} w-[80%] h-[5rem] border border-transparent shadow-md rounded-tl-md rounded-br-md pl-3 ${ingredients?.length === 0 && noteFlag === true ? "mb-[16rem]" : null}`}
        onChange={(e) => setNoteContent(e.target.value)}
      />
      <div className={`grid justify-items-center w-full gap-y-2 ${ingredients?.length === 1 ? "mb-[12rem]" : ingredients?.length === 2 ? "mb-[8rem]" : ingredients?.length === 3 ? "mb-[4rem]" : null }`}>
        {
          ingredients?.map((item:any, index:any) => (
            <div className="flex justify-between items-center bg-white border border-transparent shadow-md rounded-md w-[80%] px-2 h-[3.4rem]" key={index}>
              <div className="flex items-center gap-x-2">
                <Image src={item.src} alt="ingredients_image" width={50} height={50}/>
                <h1 className="text-md tracking-widest">{item.name}</h1>
              </div>
              <AiOutlineCloseCircle className="text-2xl text-gray-400 cursor-pointer" onClick={() => handleClose(item)}/>
            </div>
          ))  
        }
      </div>
      <div className="flex justify-center items-center gap-x-4 sticky bottom-0 w-[80%] z-50 bg-C6 py-3">
        {
          placeOrderLoad === true ? 
            <div className="border border-C3 rounded-t-[1.8rem] rounded-b-2xl px-14 py-[0.57rem]">
              <Spin />
            </div>
            :
            <button className="border border-transparent rounded-t-[1.8rem] rounded-b-2xl px-3 py-3 tracking-[0.1rem] font-semibold text-[0.8rem] text-white bg-C3" onClick={customOrderHandler}>PLACE AN ORDER</button>
        }
        <button className="border border-C3 rounded-t-[1.7rem] rounded-b-2xl bg-C6 px-8 py-[0.84rem] tracking-[0.1rem] font-semibold text-C3 text-[0.7rem]" onClick={clearHandler}>CLEAR</button>
      </div>
    </>
  )
}

export default LeftBoard