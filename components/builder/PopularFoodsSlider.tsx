'use client'
import React, { useState, useEffect } from 'react'
import Image from 'next/image'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation } from 'swiper/modules'
import { Spin, Tooltip } from 'antd'
import { AiFillStar } from 'react-icons/ai'
import 'swiper/css'
import 'swiper/css/navigation'
import { BiSolidLeftArrow, BiSolidRightArrow } from 'react-icons/bi'
import { usePopularFoods } from '@/queries/hooks/usePopularFoods'
import { Ingredients_Product, baseUrl } from '@/api/Api_Info'
import axios from 'axios'

type Data = {
    search: string,
    ingredients: {id:number,name:string,src:string}[]|[],
    setIngredients: React.Dispatch<React.SetStateAction<[]|{id:number,name:string,src:string}[]>>,
    note: boolean,
    setNote: React.Dispatch<React.SetStateAction<boolean>>,
    setNote2: React.Dispatch<React.SetStateAction<string>>,
    setLoadIngredientsProduct: React.Dispatch<React.SetStateAction<boolean | undefined>>,
    setNameMeal: React.Dispatch<React.SetStateAction<string>>
}

const PopularFoodsSlider = ({search,ingredients,setIngredients,note,setNote,setNote2,setLoadIngredientsProduct,setNameMeal}:Data) => {

    const [token, setToken] = useState<any>()
    useEffect(() => {
        setToken(localStorage.getItem('token'))
    }, [])
    const condition = token === undefined ? false : true 
    const { data, isLoading, isError } = usePopularFoods(token,condition)

    const[name, setName] = useState("")

    const results = () => {
        if (search === "") {
            return data?.data.data
        }
        else {
            return data?.data.data.filter((item:any) => item.name.toLowerCase().startsWith(search.toLowerCase()))
        }
    }

    useEffect(() => {
        ingredients.length === 0 ? setName("") : null
    }, [ingredients])

    const clickOnPopularFoodsHandler = async (item:any) => {
        setLoadIngredientsProduct(true)
        setName(item.name)
        const token = localStorage.getItem('token')
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        }
        const ing: any = []
        await axios.get(`${baseUrl}${Ingredients_Product}/${item.id}`,config)
        .then(async function(res) {
            console.log(res)
            await res.data.data.ingredients.map(
                (ingredient:any) => 
                ing.push({id:ingredient.id,name:ingredient.name,src:ingredient.main_image})
            )
            setIngredients(ing)
            if(item.notes) {
                setNote(true)
                setNote2(item.notes)
            } 
            else {
                if(note){
                    setNote(false)
                }
            }
            setNameMeal(item.name)
            setLoadIngredientsProduct(false)
        })
        .catch(function(err) {
            console.log(err)
            setLoadIngredientsProduct(false)
        })
    }

  return (
    <div className="mt-[3%] mb-[2%] md:mx-[9%] lg:mx-[8%] xl:mx-[7%]">
        {
            isLoading === true || token === undefined ?
            <div className="flex justify-center my-[4rem]">
                <Spin />
            </div>
            :
            isError ?
            <div className="flex justify-center my-[4rem]">
                <h1 className='text-red-400 text-lg'>Error, Something went wrong...</h1>
            </div>
            :
            <>
            <button className="button-next absolute top-[34%] right-[4.5%] z-40 text-[2.8rem] text-white drop-shadow-md">
                <BiSolidRightArrow />
            </button>
            <button className="button-prev absolute top-[34%] left-[4.5%] z-40 text-[2.8rem] text-white drop-shadow-md">
                <BiSolidLeftArrow />
            </button>
            <Swiper
                slidesPerView={1}
                spaceBetween={0}
                breakpoints={{
                768: {
                    slidesPerView:3,
                },
                1024: {
                    slidesPerView:4,
                },
                1280: {
                    slidesPerView:6,
                },
                }}
                modules={[Navigation]}
                navigation={{
                    nextEl:".button-next",
                    prevEl:".button-prev"
                }}
            >
                { 
                    results()?.map((item:any, index:any) => (
                    <SwiperSlide key={index}>
                        <div className="grid justify-items-center relative group cursor-pointer" onClick={() => clickOnPopularFoodsHandler(item)}>
                            {   
                                ingredients.length === 0 ? 
                                <Image 
                                    src={item.main_image} 
                                    alt="food_image" 
                                    className="md:w-[50%] lg:w-[42%] xl:w-[55%] h-[80%] p-1 rounded-b-[4rem] rounded-t-[2.4rem]"
                                    width={100}
                                    height={100} 
                                />
                                :
                                <Tooltip
                                    title="This action will replace your ingredients with the ingredients of the selected meal"
                                    color={'orange'}
                                > 
                                <Image 
                                    src={item.main_image} 
                                    alt="food_image" 
                                    className="md:w-[50%] lg:w-[42%] xl:w-[55%] h-[80%] p-1 rounded-b-[4rem] rounded-t-[2.4rem]"
                                    width={100}
                                    height={100} 
                                />
                                </Tooltip>
                            }
                            <h1 className={`${name === item.name ? "text-C3" : null} group-hover:text-C3 mt-[-1.8rem]`}>{item.name}</h1> 
                            <div className="absolute md:left-[13%] lg:left-[20%] xl:left-[13%] flex items-center gap-x-[0.15rem] bg-white border border-transparent rounded-[3rem] px-2 py-1"> 
                                <p className="font-semibold">{item.rate}</p>
                                <AiFillStar className="text-yellow-400"/>
                            </div>
                        </div>
                    </SwiperSlide>
                    ))
                }
            </Swiper>
            </>
        }
    </div>
  )
}

export default PopularFoodsSlider