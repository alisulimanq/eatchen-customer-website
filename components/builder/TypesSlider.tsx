'use client'
import React, { useEffect, useState } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation } from 'swiper/modules'
import 'swiper/css'
import 'swiper/css/navigation'
import { BiSolidLeftArrow, BiSolidRightArrow } from 'react-icons/bi'
import { useCategories } from '@/queries/hooks/useCategories'

type Data = {
    color: string|undefined,
    setColor: React.Dispatch<React.SetStateAction<string|undefined>>,
    setTypes: React.Dispatch<React.SetStateAction<{id:number,name:string,src:string}[]|undefined>>
}

const TypesSlider = ({color,setColor,setTypes}:Data) => {

    const [token, setToken] = useState<any>()
    useEffect(() => {
        setToken(localStorage.getItem('token'))
    }, [])
    const condition = token === undefined ? false : true 
    const { data } = useCategories(token,condition)

    const categoryHandler = (item:any) => {
        const types:{id:number,name:string,src:string}[] = []
        item.ingredients.map((type:any) => types.push({id:type.id,name:type.translation.name,src:type.main_image.original_url}))
        setTypes(types) 
        setColor(item.translation.name)    
    }

  return (
    <div className="w-full border py-3 shadow-md bg-white sticky bottom-0 z-50 px-6">
        <button className="button-next-2 absolute top-[14%] right-[0%] z-50 p-1 text-[1.7rem] text-C3">
            <BiSolidRightArrow />
        </button>
        <button className="button-prev-2 absolute top-[14%] left-[0%] z-50 p-1 text-[1.7rem] text-C3">
            <BiSolidLeftArrow />
        </button>
        <Swiper
            slidesPerView={1}
            spaceBetween={0}
            breakpoints={{
            768:{
                slidesPerView:4
            },
            1024: {
                slidesPerView:3
            },
            1280: {
                slidesPerView:4
            },
            }}
            modules={[Navigation]}
            navigation={{
                nextEl:".button-next-2",
                prevEl:".button-prev-2"
            }}
        >
            { 
                data?.data.data.map((item:any, index:number) => (
                <SwiperSlide key={index}>
                    <div className="flex justify-center">
                        <h1 className={`w-full flex justify-center hover:text-C3 md:text-[1rem] lg:text-[0.9rem] xl:text-[1rem] cursor-pointer ${color === item.name ? "text-C3":"text-gray-400"}`} onClick={() => categoryHandler(item)}>{item.translation.name}</h1>  
                    </div>
                </SwiperSlide>
                ))
            }
        </Swiper>
    </div>
  )
}

export default TypesSlider