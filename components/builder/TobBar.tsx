import React from 'react'

type Data = {
    search: string,
    setSearch: (search:string) => void
}

const TobBar = ({search, setSearch}:Data) => {
  return (
    <div className="flex justify-between px-[4%]">
        <h1 className="text-3xl tracking-[0.4rem] font-semibold">POPULAR FOODS</h1>
        <input 
            type="text"
            value={search}
            placeholder="Search..."
            className="pl-[1.8%] py-2 px-[10%] text-[1.3rem] border border-transparent rounded-md md:w-[35%] lg:w-[45%] shadow-md"
            onChange={(e) => setSearch(e.target.value)}
        />
    </div>
  )
}

export default TobBar