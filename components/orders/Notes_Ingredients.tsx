import React from 'react'
import Image from 'next/image'
import { motion } from 'framer-motion'

type Data = {
    details: number | undefined,
    orderId: number,
    orderNotes: string,
    orderIngredients: any
}

const Notes_Ingredients = ({details,orderId,orderNotes,orderIngredients}:Data) => {
  return (
    <>
    {
        details === orderId &&
        <>
            <motion.p
                initial={{opacity:0,x:-10}}
                animate={{opacity:1,x:0}}
                transition={{duration:0.3}}
                className="col-span-12"
            >
                {orderNotes}
            </motion.p>
            <motion.div
                initial={{opacity:0,x:-10}}
                animate={{opacity:1,x:0}}
                transition={{duration:0.3}}
                className="grid grid-cols-12 gap-x-2 w-[50%] col-span-12"
            >
                {orderIngredients.map((ingredient:any) =>
                    (
                        <>
                            <div className="md:col-span-3 lg:col-span-2">
                                <div className="flex justify-center">
                                    <Image src={ingredient.main_image} alt="shef_photo" width={40} height={40} className="w-10 h-10" />  
                                </div>
                                <h1 className="text-center break-words">{ingredient.name}</h1>
                            </div>
                        </>
                    )
                )}
            </motion.div>
        </>
    }
    </>
  )
}

export default Notes_Ingredients