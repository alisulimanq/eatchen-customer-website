'use client'
import React from 'react'
import Link from 'next/link'
import { usePathname } from 'next/navigation' 

const Navbar2 = () => {

  const pathname = usePathname()

  return (
    <main className="flex justify-between items-center bg-C6 pt-6 pb-4 px-[4%]">
      <h1 className="text-4xl text-C3 font-bold tracking-wider">Eatchen</h1>
      <div className="xs:hidden md:flex justify-evenly items-center md:gap-x-8 lg:gap-x-16">
        <Link href="/" className="h-fit px-3 pb-1 text-C4 border-[3px] border-transparent" >Home</Link>
        <Link href="/about" className={`h-fit px-3 pb-1 text-C4 border-[3px] border-transparent ${pathname === "/about" ? "border-b-C3":null}`} >About</Link>
        <Link href="/login" className={`py-2 px-10 text-black border-2 border-transparent rounded-b-xl rounded-t-3xl ${pathname === "/about" ? "bg-C6" : "bg-C3"}`} >GET IN</Link>
      </div> 
    </main>
  )
}

export default Navbar2