import { useQuery } from 'react-query'
import axios from 'axios'
import { baseUrl, user_orders } from '@/api/Api_Info'

const config = (token:string) => {
    return {
        headers: { Authorization: `Bearer ${token}` }
    }
}

const fetchOrders = (id:number,token:string) => {
    return axios.get(`${baseUrl}${user_orders}/${id}`,config(token))
}

export const useOrders = (id:number,token:string,condition:boolean) => {
    return useQuery('Orders',() => fetchOrders(id,token),
    {
        refetchOnWindowFocus: false,
        enabled: condition
    }
    )
}
