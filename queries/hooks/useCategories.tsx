import { useQuery } from 'react-query'
import axios from 'axios'
import { baseUrl, ingredients } from '@/api/Api_Info'

const config = (token:string) => {
    return {
        headers: { Authorization: `Bearer ${token}` }
    }
}

const fetchCategories = (token:string) => {
    return axios.get(`${baseUrl}${ingredients}`,config(token))
}

export const useCategories = (token:string,condition:boolean) => {
    return useQuery('Categories',() => fetchCategories(token),
    {
        refetchOnWindowFocus: false,
        enabled: condition
    }
    )
}
