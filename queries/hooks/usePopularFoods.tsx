import { useQuery } from 'react-query'
import axios from 'axios'
import { baseUrl, popular_products } from '@/api/Api_Info'

const config = (token:string) => {
    return {
        headers: { Authorization: `Bearer ${token}` }
    }
}

const fetchPopularFoods = (token:string) => {
    return axios.get(`${baseUrl}${popular_products}`,config(token))
}

export const usePopularFoods = (token:string,condition:boolean) => {
    return useQuery('PopularFoods',() => fetchPopularFoods(token),
    {
        refetchOnWindowFocus: false,
        enabled: condition
    }
    )
}