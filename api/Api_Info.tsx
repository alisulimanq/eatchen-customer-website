export const baseUrl = "http://ec2-18-234-99-215.compute-1.amazonaws.com/"

export const loginUrl = "api/v1/auth/login"

export const signupUrl = "api/v1/auth/register"

export const ingredients = "api/v1/categories/ingredients"

export const popular_products = "api/v1/products/popular"

export const Ingredients_Product = "api/v1/products"

export const custom_order = "api/v1/orders/store_custom"

export const user_orders = "api/v1/orders/user_orders"

export const verify_email = "api/v1/auth/forget_password"

export const verify_code = "api/v1/auth/verify_code"

export const change_password = "api/v1/auth/reset_password"

