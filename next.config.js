/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ['ec2-18-234-99-215.compute-1.amazonaws.com'],
    },
}

module.exports = nextConfig
