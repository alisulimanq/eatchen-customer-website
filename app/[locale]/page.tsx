'use client'
import Image from "next/image"
import bg_orange from "@/public/bg2.png"
import burger from "@/public/slider.png"
import arrow from "@/public/50.png"
import Link from "next/link"
import { usePathname } from "next/navigation"
import { motion } from 'framer-motion'

const Main = () => {

  const pathname = usePathname()
  
  return(
    <main className="xs:hidden md:grid grid-cols-12 bg-C6">
      <section className="col-span-6 pl-[15%] pt-[2.5rem]">
        <h1 className="text-4xl text-C3 font-bold tracking-wider">Eatchen</h1>
        <motion.div
          initial={{opacity:0,x:-50}}
          animate={{opacity:1,x:0}}
          transition={{duration:0.8}} 
          className="md:mt-[30%] lg:mt-[18%]"
        >
          <p className="md:text-[4rem] lg:text-[5.5rem] font-black">Village</p>
          <p className="md:text-[4rem] lg:text-[5.5rem] font-black mt-[-5%]">Chicken</p>
        </motion.div>
        <motion.p
          initial={{opacity:0}}
          animate={{opacity:1}}
          transition={{duration:0.5}} 
          className="text-C5 md:text-[0.8rem] lg:text-sm my-[2rem] md:pl-[1rem] lg:pl-[1.6rem] xl:pl-[2rem]"
        >
          Different ordering experience...
        </motion.p>
        <motion.div 
          initial={{opacity:0}}
          animate={{opacity:1}}
          transition={{duration:0.5}} 
          className="flex gap-x-2"
        >
          <button className="md:px-[10%] lg:px-[15%] py-4 bg-C3 border border-transparent rounded-[1rem] h-fit shadow-lg">
            <h1 className="text-white border border-transparent border-b-white">Take a look</h1>
          </button>
          <Image src={arrow} alt="arrow" className="md:mt-[-13%] xl:mt-[-10%] ml-[4%] md:w-[30%] lg:w-[27%] xl:w-[20%]" />
        </motion.div> 
      </section>
      <section className="col-span-6 flex justify-end relative">
        <div className="absolute md:top-[3rem] md:right-[2rem] lg:right-[3rem] xl:right-[4rem]">
          <Link href="/" className={`px-2 pb-[0.20rem] text-black border-2 border-transparent ${pathname === "/" ? "border-b-white":null}`} >Home</Link>
          <Link href="/about" className="px-2 md:mx-[1rem] lg:mx-[3.5rem] xl:mx-[4.4rem] xxl:mx-[6rem] text-black border-2 border-transparent" >About</Link>
          <Link href="/login" className="bg-C6 py-3 px-10 text-black text-sm border-2 border-transparent rounded-b-xl rounded-t-3xl" >GET IN</Link>
        </div> 
        <Image src={bg_orange} alt="bg_orange" className="h-screen md:w-[90%] xl:w-[80%]" />
        <motion.div
          initial={{opacity:0}}
          animate={{opacity:1}}
          transition={{duration:1}}
        >
          <Image src={burger} alt="burger" className="absolute md:top-[30%] lg:top-[20%] xl:top-[20%] md:right-[10%] xl:right-[20%] md:w-[100%] xl:w-[85%]" />
        </motion.div>
      </section>
    </main>
  )
}

export default Main
