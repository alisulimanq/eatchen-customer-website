'use client'
import React, { useEffect, useState } from 'react'
import '@/app/[locale]/(main)/builder/page.css'
import TobBar from '@/components/builder/TobBar'
import PopularFoodsSlider from '@/components/builder/PopularFoodsSlider'
import LeftBoard from '@/components/builder/LeftBoard'
import RightBoard from '@/components/builder/RightBoard'
import TypesSlider from '@/components/builder/TypesSlider'
import { useCategories } from '@/queries/hooks/useCategories'
import { Spin } from 'antd'
import { redirect } from 'next/navigation'

const Builder = () => {

  const [token, setToken] = useState<any>()
  useEffect(() => {
    setToken(localStorage.getItem('token'))
  }, [])
  const condition = token === undefined ? false : true 
  const { data, isLoading, isError } = useCategories(token,condition)
  
  useEffect(() => {
    token !== undefined ? token === null ? redirect('/login') : null : null 
    data?.data.code === 401 ? redirect('/login') : null 
  }, [token,data])
  
  const[search, setSearch] = useState("")
  const[successAddOrder, setSuccessAddOrder] = useState<boolean>(false)
  const[loadIngredientsProduct, setLoadIngredientsProduct] = useState<boolean|undefined>()
  const[active, setActive] = useState<string[]>()
  const[note, setNote] = useState(false)
  const[note2, setNote2] = useState("")
  const[ingredients, setIngredients] = useState<{id:number,name:string,src:string}[]|[]>([]) 
  const[color, setColor] = useState<string>()
  const[types, setTypes] = useState<{id:number,name:string,src:string}[]>()
  const[nameMeal, setNameMeal] = useState("")

  useEffect(() => {
    if(color === undefined && data !== undefined) {
      const initiateCategory = data?.data.data[0].translation.name
      const initiateTypes: {id:number,name:string,src:string}[] = []
      data?.data.data[0].ingredients.map((ingredient:any) => initiateTypes.push({id:ingredient.id,name:ingredient.translation.name,src:ingredient.main_image.original_url}))
      setColor(initiateCategory)
      setTypes(initiateTypes)
    }
    else{
      null 
    }
  }, [data,color])
  

  useEffect(() => {
    if(ingredients.length !== 0) {
      const one = data?.data.data.filter((item:any) => item.translation.name === color)
      const two = one[0].ingredients.map((item:any) => item.translation.name)
      const three = ingredients.map((item) => item.name)
      const four = three.filter((item) => two.includes(item))
      setActive(four)
    } 
    else{
      setActive([])
    }
  }, [ingredients,color,data])

  const empty = "Select From The Right Board To Start Building Your Custom Meal, Or You Can Start With The Predefined Meals Above."
  const empty2 = "Select From The Bottom Board To Start Building Your Custom Meal, Or You Can Start With The Predefined Meals Above."

  successAddOrder === true ? redirect('/orders') : null

  return (
    <main className="mt-[2rem] xs:hidden md:block">
      <section className="mt-[3%]">
        <TobBar 
          search={search} 
          setSearch={setSearch}
        />
        <PopularFoodsSlider 
          search={search} 
          ingredients={ingredients} 
          setIngredients={setIngredients}
          note={note}
          setNote={setNote}
          setNote2={setNote2}
          setLoadIngredientsProduct={setLoadIngredientsProduct}
          setNameMeal={setNameMeal}
        />
      </section>
      <section className="grid grid-cols-12 justify-items-center mx-[4%] mb-[2rem] border border-transparent shadow-md">
        <div className="md:col-span-12 lg:col-span-7 grid justify-items-center w-full h-[60vh] no-scrollbar overflow-y-auto">
          <div className={`${loadIngredientsProduct === true ? "justify-center" : "flex-col"} w-full flex items-center gap-y-4 pt-8`}>
            {
              loadIngredientsProduct === true ? 
              <Spin />
              :
              ingredients.length === 0 ?
              <> 
              <div className="md:hidden lg:block mt-[15%] md:mx-[8%] lg:mx-[10%] xl:mx-[20%] text-C7 tracking-widest">{empty}</div>
              <div className="md:block lg:hidden mt-[15%] md:mx-[8%] lg:mx-[10%] xl:mx-[20%] text-C7 tracking-widest">{empty2}</div>  
              </>
              : 
              <LeftBoard 
                ingredients={ingredients} 
                noteFlag={note} 
                nameMeal={nameMeal} 
                setNameMeal={setNameMeal} 
                noteContent={note2} 
                setNoteContent={setNote2} 
                setNoteFalg={setNote} 
                setIngredients={setIngredients} 
                setSuccessAddOrder={setSuccessAddOrder}
              />
            } 
          </div>  
        </div>
        <div className={`md:col-span-12 lg:col-span-5 ${data === undefined ? "grid justify-center items-center" : null} w-full bg-white h-[60vh] no-scrollbar overflow-y-auto`}>
          {
            isLoading === true || token === undefined ? 
            <Spin />
            :
            isError ?
            <div className="flex justify-center my-[4rem]">
              <h1 className="text-red-400 text-lg">Error, Something went wrong...</h1>
            </div>
            :
            <>
            <RightBoard 
              types={types} 
              color={color} 
              active={active} 
              ingredients={ingredients} 
              setIngredients={setIngredients} 
            />
            <TypesSlider 
              color={color}  
              setColor={setColor} 
              setTypes={setTypes}
            />
            </> 
          }
        </div>
      </section>
    </main>
  )
}

export default Builder