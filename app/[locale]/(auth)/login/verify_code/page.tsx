'use client'
import React, { useEffect, useRef, useState } from "react"
import Image from "next/image"
import verifyCode from '@/public/undraw_forgot_password_re_hxwm 1.png'
import { Spin, notification } from "antd"
import { baseUrl, verify_code } from "@/api/Api_Info"
import axios from "axios"
import { redirect } from "next/navigation"
import '@/app/[locale]/(auth)/login/verify_code/page.css'

interface Props {}

type NotificationType = 'success' | 'info' | 'warning' | 'error'

let currentOTPIndex: number = 0

const VerifyCode = (props:Props) => {

    const [done, setDone] = useState<boolean|undefined>(undefined)
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false)
    const [isDirty, setIsDirty] = useState<boolean>(false)

    const [otp, setOtp] = useState<string[]>(new Array(6).fill(""))
    const [activeOTPIndex, setActiveOTPIndex] = useState<number>(0)

    const inputRef = useRef<HTMLInputElement>(null)

    const handleOnChange = ({target}:React.ChangeEvent<HTMLInputElement>): void => {
        const {value} = target
        const newOTP : string[] = [...otp]
        newOTP[currentOTPIndex] = value.substring(value.length -1)

        if(!value)setActiveOTPIndex(currentOTPIndex - 1)
        else setActiveOTPIndex(currentOTPIndex + 1)
        
        setOtp(newOTP) 
    }

    const handleOnKeyDown = ({ key }:React.KeyboardEvent<HTMLInputElement>, index:number) => {
        currentOTPIndex = index
        if(key === 'Backspace') setActiveOTPIndex(currentOTPIndex -1)
    }

    useEffect(() => {
        inputRef.current?.focus()
    }, [activeOTPIndex])

    const [api, contextHolder] = notification.useNotification()

    const openNotificationWithIcon = (type: NotificationType, message:string) => {
        api[type]({
            message,
        })
    } 

    const onSubmit = async (e:React.FormEvent<HTMLFormElement>) => {
        setIsSubmitting(true)
        e.preventDefault()
        let otpLength: number[] = []
        otp.map((value) => value !== "" ? otpLength.push(1) : null)
        let code: string = ""
        if(otpLength.length === 6) {
            otp.map((item) => code = (code + item))
            await axios.post(`${baseUrl}${verify_code}`,{
                email: localStorage.getItem('email'),
                reset_password_code: code
            })
            .then(function(res) {
                if (res.data.success) {
                    setDone(true)
                    localStorage.setItem('forget_password', res.data.data)
                }
                else{
                    setDone(false)
                    setIsSubmitting(false)
                }
                console.log(res)
            })
            .catch(function(err) {
                setDone(false)
                setIsSubmitting(false)
                console.log(err)
            }) 
        } 
        else {
            setIsSubmitting(false)
            openNotificationWithIcon("error","Error, you must fill in all the code fields")
        }  
    }

    done === true ? redirect('/login/change_password') : 
    done === false ? openNotificationWithIcon("error","Error, something went wrong") : null   

    useEffect(() => {
        let code2 : string = "" 
        otp.map((item) => code2 = item + code2) 
        code2 === "" ? setIsDirty(false) : setIsDirty(true)    
    }, [otp])

    return (
        <main>
            {contextHolder}
            <div className="grid grid-cols-12 md:gap-x-8 xl:gap-x-0 content-center md:pt-[22%] lg:pt-[12%] xxl:pt-[10%] md:px-[4%] xxl:px-[10%]">
                <section className="md:col-span-5 xl:col-span-6">
                    <Image src={verifyCode} alt="verify_code_image" priority={true} className="md:w-[95%] lg:w-[85%] xl:w-[65%] md:mt-[-6%] md:ml-[7%] xl:ml-[16%]" />
                </section>
                <section className="md:col-span-7 xl:col-span-6">
                    <form className="grid md:gap-y-6 lg:gap-y-10 bg-white border border-transparent shadow-md rounded-3xl px-[8%] py-[8%]"
                        onSubmit={onSubmit} 
                    >
                        <div className="flex justify-center items-center md:gap-x-3">
                            <h1>Code</h1>
                            {
                                otp.map((_, index) => {
                                    return (
                                        <React.Fragment key={index}>
                                            <input
                                                ref={index === activeOTPIndex ? inputRef : null}
                                                type="number"
                                                className="md:w-9 md:h-9 lg:w-12 lg:h-12 border-2 rounded bg-transparent outline-none
                                                            text-center font-semibold text-xl spin-button-none 
                                                            border-gray-400 focus:border-gray-700 focus:text-gray-700
                                                            text-gray-400 transition"
                                                onChange={handleOnChange}
                                                onKeyDown={(e) => handleOnKeyDown(e, index)}
                                                value={otp[index]}
                                            />
                                        </React.Fragment>
                                    )
                                })
                            }
                        </div>
                        <div className="flex justify-center">
                            {   isSubmitting === true ? 
                                    <Spin />
                                    :
                                    isSubmitting === false ? 
                                        <button 
                                            disabled={!isDirty}
                                            className="bg-C3 text-white md:py-1 lg:py-[0.4rem] h-fit border border-transparent rounded-b-lg rounded-t-3xl shadow-md md:w-[40%] xl:w-[35%]"
                                        >
                                            Verify
                                        </button>
                                        :
                                            null
                            } 
                        </div>
                    </form>
                </section>
            </div>
        </main>
    )
}

export default VerifyCode




























