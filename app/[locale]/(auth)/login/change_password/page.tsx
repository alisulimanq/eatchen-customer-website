'use client'
import React, { useState } from 'react'
import Image from "next/image"
import Link from "next/link"
import { useForm } from "react-hook-form"
import { zodResolver } from "@hookform/resolvers/zod"
import { schema } from "@/schema/ChangePasswordSchema"
import { AiOutlineEye } from "react-icons/ai" 
import { AiOutlineEyeInvisible } from "react-icons/ai"
import { Spin, notification } from "antd"
import { redirect } from "next/navigation"
import axios from 'axios'
import { baseUrl, change_password } from '@/api/Api_Info'
import changePassword from '@/public/undraw_sign_up_n6im 1.png'
import { useTranslations } from 'next-intl'
import Link2 from 'next-intl/link'
import '@/app/[locale]/(auth)/login/verify_code/page.css'

type FormData = {
    password: string,
    confirmPassword: string
}

type Params = {
    locale: string
}

type NotificationType = 'success' | 'info' | 'warning' | 'error'

export default function ChangePassword({params}:{params:Params}) { 

    const t = useTranslations('change_password')

    const { locale } = params

    const [secure, setSecure] = useState(true)
    const [secure2, setSecure2] = useState(true)
    const [done, setDone] = useState<boolean|undefined>(undefined)
    const { register, handleSubmit, formState } = useForm<FormData>({
        resolver: zodResolver(schema),
    })
    const { errors, isDirty, isSubmitting, isSubmitSuccessful } = formState

    const onSubmit = async ({password}:FormData) => {  
        await axios.post(`${baseUrl}${change_password}`,{
            email: localStorage.getItem('email'),
            reset_password_token: localStorage.getItem('forget_password'),
            password
        })
        .then(function(res) {
            if (res.data.success === undefined) {
                setDone(true)
            }
            else{
                setDone(false)
            }
            console.log(res)
        })
        .catch(function(err) {
            setDone(false)
            console.log(err)
        })
    }

    const handleSecure = () => {
        setSecure(!secure)
    }

    const handleSecure2 = () => {
        setSecure2(!secure2)
    }

    const [api, contextHolder] = notification.useNotification();

    const openNotificationWithIcon = (type: NotificationType, message:string) => {
        api[type]({
            message,
        })
    }

    if (isSubmitSuccessful == true && done == true){
        redirect("/login/welcome_back")
    }
    else if(isSubmitSuccessful == true && done == false){
        openNotificationWithIcon("error","Error, something went wrong")
    }

    return(
        <main> 
        {contextHolder}
            <div className="grid grid-cols-12 md:gap-x-8 xl:gap-x-0 content-center md:pt-[17%] lg:pt-[11%] xxl:pt-[9%] md:px-[4%] xxl:px-[10%]">
                <section className="md:col-span-5 xl:col-span-6">
                    <Image src={changePassword} alt="change_password_image" priority={true} className="md:pt-[4%] md:ml-[10%] lg:ml-[15%] xl:pt-[0%] xl:w-[80%] xl:ml-[18%] xxl:w-[83%] xxl:ml-[15%] xxl:pt-[0%]" />
                </section>
                <section className="md:col-span-7 xl:col-span-6">
                    <form className="grid md:gap-y-2 lg:gap-y-4 bg-white border border-transparent shadow-md rounded-3xl px-[8%] py-[8%]"
                        onSubmit={handleSubmit(onSubmit)} 
                        noValidate
                    >

                        <div className="flex items-center relative">
                            <h1 className="md:basis-[35%] lg:basis-[20%] xl:basis-[18%] text-sm">{t('password_label')}</h1>
                            <input type={secure == false ? "text" : "password"} className="p-2 bg-gray-200 border border-transparent text-black w-full"
                                {...register("password")}
                            />
                            {
                                secure == false ?
                                    <AiOutlineEye className="absolute top-[10px] right-3 text-2xl text-gray-400" onClick={handleSecure}/>
                                    :
                                    <AiOutlineEyeInvisible className="absolute top-[10px] right-3 text-2xl text-gray-400" onClick={handleSecure}/>
                            }
                        </div>

                        <h1 className="text-red-600">{errors.password?.message}</h1>

                        <div className="flex items-center relative">
                            <h1 className="md:basis-[35%] lg:basis-[20%] xl:basis-[18%] text-sm">{t('confirm_password_label')}</h1>
                            <input type={secure2 == false ? "text" : "password"} className="p-2 bg-gray-200 border border-transparent text-black w-full"
                                {...register("confirmPassword")}
                            />
                            {
                                secure2 == false ?
                                    <AiOutlineEye className="absolute top-[10px] right-3 text-2xl text-gray-400" onClick={handleSecure2}/>
                                    :
                                    <AiOutlineEyeInvisible className="absolute top-[10px] right-3 text-2xl text-gray-400" onClick={handleSecure2}/>
                            }
                        </div>

                        <h1 className="text-red-600">{errors.confirmPassword?.message}</h1>

                        <div className="flex justify-center items-center mt-[4%]">
                            {   isSubmitting === true ? 
                                    <Spin />
                                    :
                                    <button 
                                        disabled={!isDirty||isSubmitting}
                                        className="bg-C3 text-white md:px-6 lg:px-8 xl:px-11 py-[0.4rem] h-fit border border-transparent rounded-b-lg rounded-t-3xl shadow-md md:w-[40%] lg:w-auto"
                                    >
                                        {t('reset_button')}
                                    </button>
                            }
                        </div>

                    </form>
                </section>
            </div>
        </main>
    )
}