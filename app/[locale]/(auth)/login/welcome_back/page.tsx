import React from 'react'
import Image from 'next/image'
import welcome_back from '@/public/undraw_login_rsse_4vu2 1.png'
import Link from 'next/link'

const WelcomeBack = () => {
    return (
        <main>
            <section className="grid justify-items-center gap-y-14 py-[4%]">
                <h1 className="md:text-3xl lg:text-4xl xl:text-5xl">Welcome back Anas!</h1>
                <Image src={welcome_back} alt="welcome_back_image" className="md:w-[55%] xl:w-[45%] xxl:w-[35%] "/>
                <Link 
                    href="/login" 
                    className="md:py-2 md:px-10 lg:py-[0.6rem] lg:px-14 text-white bg-C3 border-2 border-transparent rounded-b-xl rounded-t-3xl" 
                >
                    GET IN
                </Link>
            </section>
        </main>
    ) 
}

export default WelcomeBack