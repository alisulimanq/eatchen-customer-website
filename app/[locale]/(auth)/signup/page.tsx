'use client'
import React, { useState } from 'react'
import Image from "next/image"
import Link from "next/link"
import { useForm } from "react-hook-form"
import { zodResolver } from "@hookform/resolvers/zod"
import { schema } from "@/schema/SignupSchema"
import { AiOutlineEye } from "react-icons/ai" 
import { AiOutlineEyeInvisible } from "react-icons/ai"
import { notification } from "antd"
import { redirect } from "next/navigation"
import axios from 'axios'
import { baseUrl } from '@/api/Api_Info'
import { signupUrl } from '@/api/Api_Info'
import signup from '@/public/undraw_sign_up_n6im 1.png'
import {useTranslations} from 'next-intl'
import Link2 from 'next-intl/link'

type FormData = {
  username: string,
  email: string,
  password: string,
  confirmPassword: string
}

type Params = {
  locale: string
}

type NotificationType = 'success' | 'info' | 'warning' | 'error'

export default function Signup({params}:{params:Params}) { 

  const t = useTranslations('sign_up')

  const { locale } = params

  const [secure, setSecure] = useState(true)
  const [secure2, setSecure2] = useState(true)
  const [done, setDone] = useState<boolean|undefined>(undefined)
  const { register, handleSubmit, formState } = useForm<FormData>({
    resolver: zodResolver(schema),
  })
  const { errors, isDirty, isSubmitting, isSubmitSuccessful } = formState

  const onSubmit = async ({username,email,password}:FormData) => {  
    await axios.post(`${baseUrl}${signupUrl}`,{
      username,
      email,
      password,
      name: username[0].toUpperCase() + username.slice(1)
    })
    .then(function(res) {
      if (res.data.success) {
        setDone(true)
      }
      else{
        setDone(false)
      }
      console.log(res)
    })
    .catch(function(err) {
      setDone(false)
      console.log(err)
    })
  }

  const handleSecure = () => {
    setSecure(!secure)
  }

  const handleSecure2 = () => {
    setSecure2(!secure2)
  }

  const [api, contextHolder] = notification.useNotification();
  
  const openNotificationWithIcon = (type: NotificationType, message:string) => {
    api[type]({
      message,
    })
  }

  if (isSubmitSuccessful == true && done == true){
    redirect("/login")
  }
  else if(isSubmitSuccessful == true && done == false){
    openNotificationWithIcon("error","Error, something went wrong")
  }
  
  return(
    <main> 
      {contextHolder}
      <div className="grid grid-cols-12 md:gap-x-8 xl:gap-x-0 content-center md:pt-[12%] lg:pt-[8%] xxl:pt-[7%] md:px-[4%] xxl:px-[10%]">
        <section className="md:col-span-5 xl:col-span-6">
          <Image src={signup} alt="signup_image" priority={true} className="md:pt-[24%] md:ml-[10%] xl:pt-[7%] xl:w-[80%] xl:ml-[18%] xxl:w-[85%] xxl:ml-[12%] xxl:pt-[5%]" />
        </section>
        <section className="md:col-span-7 xl:col-span-6">
          <form className="grid md:gap-y-2 lg:gap-y-4 bg-white border border-transparent shadow-md rounded-3xl px-[8%] py-[8%]"
            onSubmit={handleSubmit(onSubmit)} 
            noValidate
          >
            
            <div className="flex items-center">
              <h1 className="md:basis-[35%] lg:basis-[20%] xl:basis-[18%] text-sm">{t('username_label')}</h1>
              <input type="text" className="p-2 bg-gray-200 border border-transparent text-black w-full"
                {...register("username")}
              />
            </div>

            <h1 className="text-red-600">{errors.username?.message}</h1>

            <div className="flex items-center">
              <h1 className="md:basis-[35%] lg:basis-[20%] xl:basis-[18%] text-sm">Email</h1>
              <input type="email" className="p-2 bg-gray-200 border border-transparent text-black w-full"
                {...register("email")}
              />
            </div>

            <h1 className="text-red-600">{errors.email?.message}</h1>

            <div className="flex items-center relative">
              <h1 className="md:basis-[35%] lg:basis-[20%] xl:basis-[18%] text-sm">password</h1>
              <input type={secure == false ? "text" : "password"} className="p-2 bg-gray-200 border border-transparent text-black w-full"
                {...register("password")}
              />
              {
                secure == false ?
                <AiOutlineEye className="absolute top-[10px] right-3 text-2xl text-gray-400" onClick={handleSecure}/>
                :
                <AiOutlineEyeInvisible className="absolute top-[10px] right-3 text-2xl text-gray-400" onClick={handleSecure}/>
              }
            </div>

            <h1 className="text-red-600">{errors.password?.message}</h1>

            <div className="flex items-center relative">
              <h1 className="md:basis-[35%] lg:basis-[20%] xl:basis-[18%] text-sm">{t('confirm_password_label')}</h1>
              <input type={secure == false ? "text" : "password"} className="p-2 bg-gray-200 border border-transparent text-black w-full"
                {...register("confirmPassword")}
              />
              {
                secure == false ?
                <AiOutlineEye className="absolute top-[10px] right-3 text-2xl text-gray-400" onClick={handleSecure}/>
                :
                <AiOutlineEyeInvisible className="absolute top-[10px] right-3 text-2xl text-gray-400" onClick={handleSecure}/>
              }
            </div>

            <h1 className="text-red-600">{errors.confirmPassword?.message}</h1>

            <div className="md:mt-4 md:grid md:justify-items-center md:gap-y-4 lg:flex lg:justify-between lg:items-center pl-[17%]">
              <button 
                disabled={!isDirty||isSubmitting} className="bg-C3 text-white md:px-6 lg:px-8 py-[0.4rem] h-fit border border-transparent rounded-b-lg rounded-t-3xl shadow-md md:w-[40%] lg:w-auto">
                {t('sign_up_button')}
              </button>
              {/* <p className="text-balck md:text-[0.8rem] lg:text-[0.7rem] xl:text-[0.9rem] font-semibold">
                {t('login_link')}
                <Link href="/signup" className="text-C3 ml-1">{t('register_link')}</Link>
              </p> */}
              <p className="text-black md:text-[0.9rem] lg:text-[0.8rem] font-semibold">{t('have_account_statement')}<Link href="/login" className="text-C3 ml-1">{t('login_link')}</Link></p>
            </div>

          </form>
        </section>
      </div>
    </main>
  )
}

{/* <main> 
{contextHolder}
<div className="grid grid-cols-12 h-screen">
  <div className={`xs:col-span-12 md:col-span-6 lg:col-span-5 ${locale === "en" ? "xs:pl-7 lg:pl-12 xl:pl-24" : "xs:pr-7 lg:pr-12 xl:pr-24"} xs:pt-[8%]`}>
      <div className={`flex justify-end gap-x-2 ${locale === "en" ? "pr-[5%]" : "pl-[5%]"} xs:mb-[8%] xl:xs:mb-[5%] text-white`}>
        <Link2 href="/signup" locale="en" className="bg-C1 px-2">en</Link2>
        <Link2 href="/signup" locale="ar" className="bg-C1 px-2">ar</Link2>
      </div>
      <h1 className="xs:text-4xl lg:text-4xl xl:text-6xl xs:mb-[7%]">{t('brand_name')}</h1>
      <form className="grid xs:gap-3 md:gap-5 lg:gap-7 xs:w-10/12 md:w-9/12 lg:w-10/12 xl:w-9/12"
        onSubmit={handleSubmit(onSubmit)} 
        noValidate
      >
        <label htmlFor="username" className="text-gray-400">
          {t('username_label')}
          <div>
            <input type="text" id="username" className="p-2 border border-gray-300 text-black w-full"
            {...register("username")}
            />  
            <h1 className="text-red-600">{errors.username?.message}</h1>
          </div>
        </label>
  
        <label htmlFor="email" className="text-gray-400">
          {t('email_label')}
          <div>
            <input type="email" id="email" className="p-2 border border-gray-300 text-black w-full"
            {...register("email")}
            />
            <h1 className="text-red-600">{errors.email?.message}</h1>
          </div>
        </label>
        
        <label htmlFor="password" className="relative text-gray-400" >
          {t('password_label')}
          <div className="realtive">
            <input type={secure == false ? "text" : "password"} id="password" className="p-2 pr-14 border border-gray-300 text-black w-full"
            {...register("password")}
            />
            {
              secure == false ?
              <AiOutlineEye className="absolute top-[33px] right-3 text-2xl" onClick={handleSecure}/>
              :
              <AiOutlineEyeInvisible className="absolute top-[33px] right-3 text-2xl" onClick={handleSecure}/>
            }
            <h1 className="text-red-600">{errors.password?.message}</h1>
          </div>
        </label>

        <label htmlFor="confirm_password" className="relative text-gray-400">
          {t('confirm_password_label')}
          <div className="realtive">
            <input type={secure2 == false ? "text" : "password"} id="confirm_password" className="p-2 pr-14 mb-3 border border-gray-300 text-black w-full"
            {...register("confirmPassword")}
            />
            {
              secure2 == false ?
              <AiOutlineEye className="absolute top-[33px] right-3 text-2xl" onClick={handleSecure2}/>
              :
              <AiOutlineEyeInvisible className="absolute top-[33px] right-3 text-2xl" onClick={handleSecure2}/>
            }
            <h1 className="text-red-600">{errors.confirmPassword?.message}</h1>
          </div>
        </label>
        <button disabled={!isDirty||isSubmitting} className="mt-2 mb-10 bg-C1 text-white py-[0.5rem] w-[40%]">{t('sign_up_button')}</button>
      </form> 
      <p className="text-gray-400">{t('have_account_statement')}<Link href="/login" className="text-C1">{t('login_link')}</Link></p>
  </div>
  <div className="xs:hidden md:flex md:col-span-6 lg:col-span-7 bg-gray-400 relative">
    <Image src={randomImage} alt="food_image" fill={true} object-fit="cover" sizes=''/>
  </div>
</div>
</main> */}